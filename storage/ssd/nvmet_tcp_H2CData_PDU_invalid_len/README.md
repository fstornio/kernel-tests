# storage/ssd/nvmet_tcp_H2CData_PDU_invalid_len

Storage: nvmet_tcp test with invalid len of H2CData_PDU

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
