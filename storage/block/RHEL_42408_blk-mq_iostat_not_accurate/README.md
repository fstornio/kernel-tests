# storage/block/RHEL_42408_blk-mq_iostat_not_accurate

Storage: blk-mq iostat/util% is not accurate sometimes

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
